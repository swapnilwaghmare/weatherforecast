//
//  WeatherWebservice.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 27/07/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WeatherWebservice;


@protocol WeatherProtocol <NSObject>

-(void)Success:(NSDictionary *)dictJSON;
-(void)Failure :(NSError *)error;

@end

@interface WeatherWebservice : NSObject

@property (weak ,nonatomic ) id <WeatherProtocol> delegate;
-(void)callWebServicesWithParameters:(NSString *)cityName withCountOfdays:(int)numberOfDays;

@end
