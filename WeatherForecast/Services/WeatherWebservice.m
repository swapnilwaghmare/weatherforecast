//
//  WeatherWebservice.m
//  WeatherForecast
//
//  Created by Cellpointmobile on 27/07/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import "WeatherWebservice.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "MBProgressHUD.h"


@implementation WeatherWebservice



-(void)callWebServicesWithParameters:(NSString *)cityName withCountOfdays:(int)numberOfDays
{
    
    NSString *strWeather = [NSString stringWithFormat:@"%@?q=%@&cnt=%d&APPID=%@",BASEURL,cityName,numberOfDays,APIKEY];
    strWeather = [strWeather stringByReplacingOccurrencesOfString:@" " withString:@""];
    
   // NSURL *urlWeather = [NSURL URLWithString:strWeather];
     NSDictionary *parameters = @{@"format": @"json"};
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager GET:[strWeather stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] parameters:parameters success:
    ^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSDictionary *dictResponse = (NSDictionary *)responseObject;
        if ([[dictResponse valueForKey:@"cod"] integerValue] == 200) {
               [self.delegate Success:dictResponse];
        }
        else
        {
              NSDictionary *errordict = @{@"message": [dictResponse valueForKey:@"message"]};
            NSError *error = [NSError errorWithDomain:nil code:nil userInfo:errordict];
            [self.delegate Failure:error];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [self.delegate Failure:error];
    }];
    
}
@end
