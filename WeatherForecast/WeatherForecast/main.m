//
//  main.m
//  WeatherForecast
//
//  Created by Cellpointmobile on 26/07/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
