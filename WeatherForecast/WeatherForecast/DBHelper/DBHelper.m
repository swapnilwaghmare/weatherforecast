//
//  DBHelper.m
//  WeatherForecast
//
//  Created by Cellpointmobile on 06/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import "DBHelper.h"
#import "CityWeather.h"
#import "CityWeather+CoreDataProperties.h"
#import "Constant.h"
#import <UIKit/UIKit.h>

@implementation DBHelper

+ (DBHelper *)sharedInstance
{
    static DBHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DBHelper alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.Swapnil.WeatherForecast" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"WeatherForecast" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"WeatherForecast.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}



-(void)insertIntoDatabaseWeatherDetails:(WeatherDetails *)wDetails forCityname:(NSString *)cityName
{
    NSManagedObjectContext *currentContext = [self managedObjectContext];
    CityWeather *weather = [NSEntityDescription insertNewObjectForEntityForName:TABLENAMECITYWEATHER inManagedObjectContext:currentContext];
    weather.temp = wDetails.temp;
    weather.date = wDetails.date;
    weather.cityName = cityName;
    NSError * error = nil;
    if (![currentContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}


-(NSArray *)fetchWeatherForCityNamed:(NSString *)cityName
{
    NSManagedObjectContext *currentContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityCatagory = [NSEntityDescription entityForName:TABLENAMECITYWEATHER
                                                      inManagedObjectContext:currentContext];
    [fetchRequest setEntity:entityCatagory];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"cityName == %@",cityName]];
    
    NSError *errorFetch = [[NSError alloc] initWithDomain:@"DATABASE FETCHING" code:400 userInfo:nil];

   NSArray *arrFetchedObjects =  [currentContext executeFetchRequest:fetchRequest error:&errorFetch];
   
    return arrFetchedObjects;
}


-(void)deleteAllRecords
{
    NSManagedObjectContext *currentContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityCatagory = [NSEntityDescription entityForName:TABLENAMECITYWEATHER
                                                      inManagedObjectContext:currentContext];
    [fetchRequest setEntity:entityCatagory];
    
    NSError *errorDELETE = [[NSError alloc] initWithDomain:@"DATABASE DELETING" code:400 userInfo:nil];
    
    NSArray *arrFetchedObjects =  [currentContext executeFetchRequest:fetchRequest error:&errorDELETE];
   
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchRequest];
        
        [[self persistentStoreCoordinator] executeRequest:delete withContext:currentContext error:&errorDELETE];
  
    }
    else
    {
        if (arrFetchedObjects.count > 0) {
            for (CityWeather *weather in arrFetchedObjects) {
                [currentContext deleteObject:weather];
            }
        }
    }
    
    if (![currentContext save:&errorDELETE]) {
        NSLog(@"Whoops, couldn't Delete: %@", [errorDELETE localizedDescription]);
    }
}

@end
