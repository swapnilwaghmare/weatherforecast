//
//  DBHelper.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 06/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "WeatherDetails.h"


@interface DBHelper : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void)insertIntoDatabaseWeatherDetails:(WeatherDetails *)wDetails forCityname:(NSString *)cityName;

+ (DBHelper *)sharedInstance;

-(NSArray *)fetchWeatherForCityNamed:(NSString *)cityName;
-(void)deleteAllRecords;
@end
