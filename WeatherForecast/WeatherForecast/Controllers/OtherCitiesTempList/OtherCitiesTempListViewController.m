//
//  OtherCitiesTempListViewController.m
//  WeatherForecast
//
//  Created by Cellpointmobile on 03/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import "OtherCitiesTempListViewController.h"
#import "CityWeather.h"

@interface OtherCitiesTempListViewController ()

@property (strong ,nonatomic) NSDateFormatter *formatter;
@end

@implementation OtherCitiesTempListViewController



#pragma mark - lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // show back button
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(actionBackClicked:)];
    self.navigationItem.leftBarButtonItem = backButton;
    //Date formatter
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateStyle:NSDateFormatterLongStyle];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark custom actions
-(void)actionBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.marrWatherDetails != nil && [self.marrWatherDetails count] > 0) {
         return [self.marrWatherDetails count];
    }
    else
    {
        return 0;
    }
   
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *weatherCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    if (self.marrWatherDetails != nil && [self.marrWatherDetails count] > 0) {
        CityWeather *wDetails = [self.marrWatherDetails objectAtIndex:indexPath.row];
        
        weatherCell.textLabel.text = [self.formatter stringFromDate:wDetails.date];
        weatherCell.detailTextLabel.text = [NSString stringWithFormat:@"%@",wDetails.temp];

    }
    else
    {
        weatherCell.textLabel.text = @"";
    }
       return weatherCell;
}


@end
