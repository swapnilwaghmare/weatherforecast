//
//  OtherCitiesTempListViewController.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 03/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherCitiesTempListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tblViewOtherCItiesTemp;
@property (strong , nonatomic) NSMutableArray *marrWatherDetails;

@end
