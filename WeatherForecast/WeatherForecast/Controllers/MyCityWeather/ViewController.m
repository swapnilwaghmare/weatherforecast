//
//  ViewController.m
//  WeatherForecast
//
//  Created by Cellpointmobile on 26/07/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import "ViewController.h"
#import "MBProgressHUD.h"
#import "WeatherWebservice.h"
#import "Constant.h"
#import <AFNetworking.h>
#import "WeatherDetails.h"
#import "OtherCitiesTempListViewController.h"
#import "DBHelper.h"
#import "WeatherForecast-Bridging-Header.h"



@interface ViewController ()<UITableViewDelegate, UITableViewDataSource,WeatherProtocol>
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    UITextField *txtFieldCityNames;
    WeatherWebservice * webService;
    NSString *selectedCity;
   
}


@end

@implementation ViewController


#pragma mark - Lifecycle
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // start updating location to get current Location
    [self locationUpdatingCustomMethod];
    
    // initiate web service for Current cities weather
    webService = [[WeatherWebservice alloc] init];
    webService.delegate = self;
    
    self.marrCities = nil;
    [self.marrCities removeAllObjects];
    NSArray *arr = [[NSUserDefaults standardUserDefaults] objectForKey:ARRAYOFCITIES];
    self.marrCities = [[NSMutableArray alloc]initWithArray:arr];
    
   
   
    
    if ([self.marrCities count] > 0) {
        [self.tblViewOtherCities reloadData];
    }
    // save todays date
    if (![[NSUserDefaults standardUserDefaults ] valueForKey:TODAYSDATE]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:TODAYSDATE];
    }
    // Do any additional setup after loading the view, typically from a nib.
}



-(void)locationUpdatingCustomMethod
{
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marks - table view methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellCityName = [tableView dequeueReusableCellWithIdentifier:@"cellCityName"];
    if (!cellCityName) {
        cellCityName = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellCityName"];
    }
    if ([self.marrCities count] > 0) {
        cellCityName.textLabel.text = [self.marrCities objectAtIndex:indexPath.row];

    }
    cellCityName.backgroundColor = self.view.backgroundColor;
    [cellCityName setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cellCityName;
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.marrCities count] > 0) {
        return [self.marrCities count];
    }
    else
    {
        return 0;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.marrCities count] > 0) {
        selectedCity = [self.marrCities objectAtIndex:indexPath.row];
        if (selectedCity) {
            NSArray *arrFetchedObjects = [[DBHelper sharedInstance] fetchWeatherForCityNamed:selectedCity];
            if ([arrFetchedObjects count] > 0) {
                [self gotoNextViewControllerWithArray:arrFetchedObjects];
            }
            else
            {
                [webService callWebServicesWithParameters:selectedCity withCountOfdays:14];
            }

        }
        

    }
}


#pragma mark - Custom Actions
- (IBAction)click_Event_FindweatherOfOtherCities:(id)sender {
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Find Weather"
                                 message:@"enter cities separated by comma"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Find"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             if ([txtFieldCityNames.text length] > 0) {
                                 txtFieldCityNames.layer.borderColor = [[UIColor blackColor] CGColor];
                                 [self actionFindClicked];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                
                             }
                             else
                             {
                                 UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter city names" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                 [alertError show];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                
                             }
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:ok];
    [view addAction:cancel];
    [view addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"enter city names separated by comma";
        txtFieldCityNames = textField;
    }];
    [self presentViewController:view animated:YES completion:nil];
    
    
}

#pragma mark - custom methods

//this method will get comma separeted values..user entered and will create array from it.
//and will load it in tableview
-(void)actionFindClicked
{
    [self.marrCities removeAllObjects];
    self.marrCities = nil;
    
    [[DBHelper sharedInstance] deleteAllRecords];
   
        NSString *concatedString =  [txtFieldCityNames.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSCharacterSet *chs = [NSCharacterSet characterSetWithCharactersInString:@"!@$%^*()+='#%^&{}[]/~|\?.<"];
   concatedString = [[concatedString componentsSeparatedByCharactersInSet:chs] componentsJoinedByString:@""];
    
   
   
        NSArray *arrCities = [concatedString componentsSeparatedByString:@","];
    self.marrCities = [[NSMutableArray alloc] initWithArray:arrCities];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:ARRAYOFCITIES];
    [defaults setObject:self.marrCities forKey:ARRAYOFCITIES];
    
    
    [self.tblViewOtherCities reloadData];
        NSLog(@"Array of cities : %@",arrCities);
}


#pragma mark CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"Detected Location : %f, %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           UIAlertView *alertFailedToGetCity = [[UIAlertView alloc] initWithTitle:@"failed to get Location!" message:[NSString stringWithFormat:@"%@",error]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                           [alertFailedToGetCity show];

                           return;
                       }
                       else
                       {
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           NSLog(@"placemark.ISOcountryCode %@",placemark.locality);
                           [self performSelectorOnMainThread:@selector(updateCUrrentLocationAndWeather:) withObject:placemark.locality waitUntilDone:YES];
                       }
                      
                       
                   }];
}

-(void)updateCUrrentLocationAndWeather:(NSString *)cityName
{
    self.lblCurrentCityName.text = cityName;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:TODAYSTEMP]) {
      // check for day change
        BOOL dateComparison = [self isSavedDateIsSmallerThanCurrent:[[NSUserDefaults standardUserDefaults] objectForKey:TODAYSDATE]];
        if (dateComparison == YES) {
            // day changed
            [webService callWebServicesWithParameters:cityName withCountOfdays:1];

        }
        else
        {
            // same day so no need for API call
           self.lblCurrentCityTemp.text =  [[NSUserDefaults standardUserDefaults] objectForKey:TODAYSTEMP];
        }
    }
    else
    {
        [webService callWebServicesWithParameters:cityName withCountOfdays:1];
    }
    
}


- (BOOL)isSavedDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
   
    if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}

#pragma mark - WeatherProtocol delegate methods


-(void)Success:(NSDictionary *)dictJSON;
{

    if ([[dictJSON valueForKey:@"cnt"] integerValue] == 1) {
        // update current city weather
        NSArray *arr = [dictJSON valueForKey:@"list"];
        NSDictionary *dictTemp = arr[0];
        NSDictionary *dictDayTemp =[dictTemp valueForKey:@"temp"];
        
        NSNumber * todaysTemp= [dictDayTemp valueForKey:@"day"] ;
   
        dispatch_async(dispatch_get_main_queue(), ^{
           
            self.lblCurrentCityTemp.text = [NSString stringWithFormat:@"%@",todaysTemp];
            [[NSUserDefaults standardUserDefaults] setObject:todaysTemp forKey:TODAYSTEMP];
        });
        
    }
    else
    {
        // create dictionaty for 14 days for user entered city
        
        NSMutableArray *marrWeatherDetails= [[NSMutableArray alloc] init];
        NSArray *arr = [dictJSON valueForKey:@"list"];
               for (int i = 0 ; i < [arr count] ; i++) {
             NSDictionary *dictTemp = arr[i];
            WeatherDetails *wDetails = [[WeatherDetails alloc] init];
            NSString *strDateInIntervalForm = [dictTemp valueForKey:@"dt"];
            NSDictionary *dictDayTemp =[dictTemp valueForKey:@"temp"];
            NSNumber * todaysTemp= [dictDayTemp valueForKey:@"day"] ;
           
            
            NSTimeInterval timeInterval = [strDateInIntervalForm doubleValue];
           NSDate * date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                   
            wDetails.date = date;
            wDetails.temp =todaysTemp;
       
            
            [marrWeatherDetails addObject:wDetails];
        }
        
       
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (WeatherDetails *wDetails in marrWeatherDetails) {
                [[DBHelper sharedInstance] insertIntoDatabaseWeatherDetails:wDetails forCityname:selectedCity];
            }
            
            NSArray *arrFetchedObjects = nil;
            if (selectedCity ) {
                arrFetchedObjects = [[DBHelper sharedInstance] fetchWeatherForCityNamed:selectedCity];
            }

            [self gotoNextViewControllerWithArray:arrFetchedObjects];
            
        });
       
        
    }
    
}


-(void)Failure :(NSError *)error
{
    NSLog(@"Failure  : %@",error);
}



-(void)gotoNextViewControllerWithArray : (NSArray *)arrFetchedObjects
{
    OtherCitiesTempListViewController *octlVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OtherCitiesTempListViewController"];
   
    if ([arrFetchedObjects count] > 0) {
        octlVC.marrWatherDetails = [[NSMutableArray alloc] initWithArray:arrFetchedObjects];
        octlVC.navigationItem.title = [NSString stringWithFormat:@"%@ Weather",selectedCity];
        [self.navigationController pushViewController:octlVC animated:YES];
  
    }
   else
   {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Something went wrong" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
       [alert show];
   }
}
@end
