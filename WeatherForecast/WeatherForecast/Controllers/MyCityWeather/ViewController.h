//
//  ViewController.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 26/07/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblCurrentCityName;

@property (strong, nonatomic) IBOutlet UILabel *lblCurrentCityTemp;

- (IBAction)click_Event_FindweatherOfOtherCities:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tblViewOtherCities;

@property (strong , nonatomic) NSMutableArray *marrCities;
@end

