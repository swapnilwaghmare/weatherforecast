//
//  CityWeather+CoreDataProperties.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 07/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CityWeather.h"

NS_ASSUME_NONNULL_BEGIN

@interface CityWeather (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *cityName;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *temp;

@end

NS_ASSUME_NONNULL_END
