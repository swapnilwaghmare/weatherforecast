//
//  CityWeather.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 07/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CityWeather : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CityWeather+CoreDataProperties.h"
