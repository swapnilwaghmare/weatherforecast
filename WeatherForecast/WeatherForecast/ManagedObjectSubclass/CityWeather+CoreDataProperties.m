//
//  CityWeather+CoreDataProperties.m
//  WeatherForecast
//
//  Created by Cellpointmobile on 07/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CityWeather+CoreDataProperties.h"

@implementation CityWeather (CoreDataProperties)

@dynamic cityName;
@dynamic date;
@dynamic temp;

@end
