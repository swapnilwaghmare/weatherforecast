//
//  WeatherDetails.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 04/08/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherDetails : NSObject
@property (strong , nonatomic) NSNumber *temp;
@property (strong , nonatomic) NSDate *date;


@end
