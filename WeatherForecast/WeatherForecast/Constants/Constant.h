//
//  Constant.h
//  WeatherForecast
//
//  Created by Cellpointmobile on 31/07/16.
//  Copyright © 2016 Swapnil. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define BASEURL @"http://api.openweathermap.org/data/2.5/forecast/daily"
#define APIKEY @"56dc74679f737b75c3ba2c40eeef8b2d"
#define TODAYSDATE @"TodaysDate"
#define TODAYSTEMP @"TodaysTemp"
#define ARRAYOFCITIES @"ArrayOfCities"
#define TABLENAMECITYWEATHER @"CityWeather"


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)



#endif /* Constant_h */
